package com.example.ejemplo1.Clases;

public class Tarea {

    private  String nombre;
    private boolean completa;

    public Tarea(String nombre, boolean completa) {
        this.nombre = nombre;
        this.completa = completa;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public boolean isCompleta() {
        return completa;
    }

    public void setCompleta(boolean completa) {
        this.completa = completa;
    }
}
