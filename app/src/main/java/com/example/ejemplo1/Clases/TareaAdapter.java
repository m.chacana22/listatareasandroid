package com.example.ejemplo1.Clases;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.ejemplo1.R;

import java.util.List;

public class TareaAdapter  extends RecyclerView.Adapter<TareaAdapter.TareaView> {

    private List<Tarea> tareaList;

    public TareaAdapter(List<Tarea> tareaList) {
        this.tareaList = tareaList;
    }

    @NonNull
    @Override
    public TareaAdapter.TareaView onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.tarea_item, parent, false);
        return new TareaView(view);
    }

    @Override
    public void onBindViewHolder(@NonNull TareaAdapter.TareaView holder, int position) {
        Tarea tarea = tareaList.get(position);
        holder.tareaNombre.setText(tarea.getNombre());
        holder.tareaCompletada.setChecked(tarea.isCompleta());
    }

    @Override
    public int getItemCount() {
       return tareaList.size();
    }

    public class TareaView extends RecyclerView.ViewHolder {
        TextView tareaNombre;
        CheckBox tareaCompletada;

        public TareaView(View itemView) {
            super(itemView);
            tareaNombre = itemView.findViewById(R.id.tarea_nombre);
            tareaCompletada = itemView.findViewById(R.id.tarea_completada);
        }
    }
}
