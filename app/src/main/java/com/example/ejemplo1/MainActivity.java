package com.example.ejemplo1;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.ejemplo1.Clases.Tarea;
import com.example.ejemplo1.Clases.TareaAdapter;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private List<Tarea> taskList = new ArrayList<>();
    private RecyclerView recyclerView;
    private TareaAdapter taskAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView = findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        taskAdapter = new TareaAdapter(taskList);
        recyclerView.setAdapter(taskAdapter);

        Button addButton = findViewById(R.id.add_button);
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Agregar una nueva tarea a la lista y notificar al adaptador
                taskList.add(new Tarea("Nueva Tarea", false));
                taskAdapter.notifyDataSetChanged();
            }
        });
    }
}
